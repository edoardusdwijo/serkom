## Tutor Instalasi Web

-   buat folder baru lalu buka terminal di folder tersebut lalu ketik perintah di bawah ini

```bash
git clone https://gitlab.com/edoardusdwijo/serkom.git
```

```bash
composer install
```

```bash
php artisan migrate
```

-   ketik yes lalu enter
-   buka cmd baru lalu ketik

```bash
php artisan serve
```
