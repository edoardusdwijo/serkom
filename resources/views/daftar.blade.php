@extends('layout/layout')

@section('title')
    Daftar
@endsection

@section('css')
    {{ asset('css/daftar.css') }}
@endsection

@section('nav')
    {{-- start nav --}}
    <nav class="navbar bg-light">
        <div class="container">
            <a class="navbar-brand" href="/">
                <img src="{{ asset('asset/logo/logo.png') }}" alt="Logo" width="auto" height="24"
                    class="d-inline-block align-text-top">
                Institut Teknologi Telkom Purwokerto
            </a>

            <ul class="nav nav-tabs justify-content-end">
                <li class="nav-item">
                    <a class="nav-link" aria-current="page" href="/">Pilihan Beasiswa</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link active" href="/daftar">Daftar</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/hasil">Hasil</a>
                </li>
            </ul>
        </div>
    </nav>
    {{-- end nav --}}
@endsection

@section('content')
    {{-- start content --}}
    <div class="daftar pb-4">
        <div class="container">
            <div class="judul">
                <p>Daftar Beasiswa</p>
            </div>
            <div class="card">
                <h5 class="card-header text-center">Registrasi Beasiswa</h5>
                <div class="card-body">
                    <form action="{{ route('regist.store') }}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="mb-3">
                            <label for="nama" class="form-label">Masukkan Nama</label>
                            <input type="text" class="form-control" id="nama" aria-describedby="emailHelp"
                                name="nama" required>
                        </div>
                        <div class="mb-3">
                            <label for="email" class="form-label">Masukkan Email</label>
                            <input type="email" class="form-control" id="email" aria-describedby="emailHelp"
                                name="email" required>
                        </div>
                        <div class="mb-3">
                            <label for="no_hp" class="form-label">Nomor HP</label>
                            <input type="number" class="form-control" id="no_hp" aria-describedby="emailHelp"
                                name="no_hp" required>
                        </div>
                        <div class="mb-3">
                            <label for="semester" class="form-label">Semester saat ini</label>
                            <select class="form-select" id="semester" onchange="updateIPK(this)" required
                                aria-label="Default select example" name="semester">
                                <option selected disabled>Pilih Semester</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                                <option value="6">6</option>
                                <option value="7">7</option>
                                <option value="8">8</option>
                            </select>
                        </div>
                        <div class="mb-3">
                            <label for="ipk" class="form-label">IPK Terakhir</label>
                            <input type="text" class="form-control" id="ipk" name="ipk" readonly>
                        </div>
                        <div class="mb-3">
                            <label for="beasiswa" class="form-label">Pilihan Beasiswa</label>
                            <select class="form-select" aria-label="Default select example" id="beasiswa" name="beasiswa"
                                disabled required>
                                <option selected disabled>Pilih Beasiswa</option>
                                <option value="Jalur Beasiswa Unggul">Jalur Beasiswa Unggul</option>
                                <option value="Beasiswa Jateng">Beasiswa Jateng</option>
                                <option value="Jalur Beasiswa Muda">Jalur Beasiswa Muda</option>
                            </select>
                        </div>
                        <div class="mb-3">
                            <label for="berkas" class="form-label">Upload Berkas Syarat</label>
                            <input class="form-control" type="file" id="berkas" name="berkas" disabled required>
                        </div>

                        <div class="mb-3 pt-3 col text-center">
                            <button type="submit" class="btn btn-primary">Daftar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    {{-- end content --}}

    {{-- JS untuk membuat random angka IPK --}}
    <script>
        function updateIPK(semesterField) {
            var ipkField = document.getElementById('ipk');
            var ipkValue = Math.random() * (3.5 - 2.5) + 2.5;
            ipkField.value = ipkValue.toFixed(2);
            var beasiswaField = document.getElementById('beasiswa');
            if (ipkValue < 3.0) {
                beasiswaField.disabled = true;
                var uploadField = document.getElementById('berkas');
                uploadField.disabled = true;
            } else {
                beasiswaField.disabled = false;
                var uploadField = document.getElementById('berkas');
                uploadField.disabled = false
            }
        }

        const fileInput = document.querySelector("#berkas");
        fileInput.addEventListener("change", () => {
            const allowedFiles = [".jpg", ".jpeg", ".pdf", ".zip"];
            const fileName = fileInput.value;
            const extension = fileName.substring(fileName.lastIndexOf(".")).toLowerCase();

            if (!allowedFiles.includes(extension)) {
                alert("File yang diupload harus dalam format JPG, PDF, atau ZIP.");
                fileInput.value = "";
            } else if (fileInput.files[0].size > 5000000) { // mengecek ukuran file, 5000000 bytes = 5 MB
                alert("Ukuran file yang diupload tidak boleh lebih dari 5 MB.");
                fileInput.value = "";
            }
        });
    </script>
    {{-- JS untuk membuat random angka IPK --}}
@endsection
