@extends('layout/layout')

@section('title')
    Pilihan Beasiswa
@endsection

@section('css')
    {{ asset('css/pilihan_beasiswa.css') }}
@endsection

@section('nav')
    {{-- start nav --}}
    <nav class="navbar bg-light">
        <div class="container">
            <a class="navbar-brand" href="/">
                <img src="{{ asset('asset/logo/logo.png') }}" alt="Logo" width="auto" height="24"
                    class="d-inline-block align-text-top">
                Institut Teknologi Telkom Purwokerto
            </a>

            <ul class="nav nav-tabs justify-content-end">
                <li class="nav-item">
                    <a class="nav-link active" aria-current="page" href="/">Pilihan Beasiswa</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/daftar">Daftar</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/hasil">Hasil</a>
                </li>
            </ul>
        </div>
    </nav>
    {{-- end nav --}}
@endsection

@section('content')
    <!-- start slider -->
    <div class="slider mb-4">
        <div id="carouselExampleDark" class="carousel carousel-dark slide" data-bs-ride="carousel">
            <div class="carousel-indicators">
                <button type="button" data-bs-target="#carouselExampleDark" data-bs-slide-to="0" class="active"
                    aria-current="true" aria-label="Slide 1"></button>
                <button type="button" data-bs-target="#carouselExampleDark" data-bs-slide-to="1"
                    aria-label="Slide 2"></button>
                <button type="button" data-bs-target="#carouselExampleDark" data-bs-slide-to="2"
                    aria-label="Slide 3"></button>
            </div>
            <div class="carousel-inner">
                <div class="carousel-item active" data-bs-interval="10000">
                    <img src="{{ asset('asset/slider/1.webp') }}" class="d-block w-100" alt="...">
                </div>
                <div class="carousel-item" data-bs-interval="2000">
                    <img src="{{ asset('asset/slider/2.webp') }}" class="d-block w-100" alt="...">
                </div>
                <div class="carousel-item">
                    <img src="{{ asset('asset/slider/3.webp') }}" class="d-block w-100" alt="...">
                </div>
            </div>
            <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleDark" data-bs-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="visually-hidden">Previous</span>
            </button>
            <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleDark" data-bs-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="visually-hidden">Next</span>
            </button>
        </div>
    </div>
    <!-- end slider -->

    <!-- start content -->
    <div class="konten container pb-4">
        <div class="card">
            <div class="card-body">
                <blockquote class="blockquote mb-0">
                    <h4>Jalur Beasiswa Unggul</h4>
                    <p>
                        Jalur Beasiswa Unggul (JBU) merupakan jalur pendaftaran di Institut Teknologi Telkom Purwokerto
                        (ITTP) yang dibuka untuk tahun akademik 2022/2023. Jalur ini memberikan banyak kesempatan beasiswa
                        melalui seleksi ujian online. Jalur Beasiswa Unggul ii dibuka terbuka pendaftarannya bagi lulusan
                        SMA/MA/SMK tahun 2019, 2020, 2021, dan 2022 dari semua jurusan: IPA/IPS/Bahasa/Agama/Teknik/Non
                        Teknik. Beasiswa hanya diberikan kepada siswa yang memperoleh nilai ujian sesuai kriteria yang
                        ditetapkan. Bagi siswa yang nilainya tidak memenuhi kriteria berarti dinyatakan tidak berhak
                        mendapatkan beasiswa dan masuk dalam skema biaya normal.
                    </p>
                </blockquote><br>
                <blockquote class="blockquote mb-0">
                    <h4>Beasiswa Jateng</h4>
                    <p>
                        Beasiswa Jateng merupakan beasiswa kuliah di Institut Teknologi Telkom Purwokerto (ITTP) yang
                        diberikan kepada siswa SMA/MA/SMK yang akan lulus tahun 2022 dari semua jurusan:
                        IPA/IPS/Bahasa/Agama/Teknik/Non Teknik. Beasiswa ini tidak berlaku bagi lulusan tahun sebelumnya
                        (Gap year). Beasiswa Jateng diberikan dalam rangka memberi kesempatan yang seluas-luasnya bagi
                        pemuda Jawa Tengah yang ingin maju dan berkembang untuk melanjutkan studinya di Institut Teknologi
                        Telkom Purwokerto (ITTP) tahun akademik 2022/2023.
                    </p>
                </blockquote><br>
                <blockquote class="blockquote mb-0">
                    <h4>Jalur Beasiswa Muda (JBM)</h4>
                    <p>
                        Jalur Beasiswa Muda (JBM) merupakan jalur pendaftaran di Institut Teknologi Telkom Purwokerto (ITTP)
                        yang dibuka untuk tahun akademik 2022/2023. Jalur ini seleksinya menggunakan nilai rapor. Jalur ini
                        dibuka untuk memberi kesempatan bagi generasi muda yang memiliki talenta yang ingin melanjutkan
                        kuliah dan meraih masa depan lebih baik. Jalur Beasiswa Muda (JBM) ini dibuka terbuka pendaftarannya
                        bagi lulusan SMA/MA/SMK tahun 2019, 2020, 2021, dan 2022 dari semua jurusan:
                        IPA/IPS/Bahasa/Agama/Teknik/Non Teknik.
                    </p>
                </blockquote>
            </div>
        </div>
    </div>
    <!-- end content -->
@endsection
