@extends('layout/layout')

@section('title')
    Hasil
@endsection

@section('css')
    {{ asset('css/hasil.css') }}
@endsection

@section('nav')
    {{-- start nav --}}
    <nav class="navbar bg-light">
        <div class="container">
            <a class="navbar-brand" href="/">
                <img src="{{ asset('asset/logo/logo.png') }}" alt="Logo" width="auto" height="24"
                    class="d-inline-block align-text-top">
                Institut Teknologi Telkom Purwokerto
            </a>

            <ul class="nav nav-tabs justify-content-end">
                <li class="nav-item">
                    <a class="nav-link" aria-current="page" href="/">Pilihan Beasiswa</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/daftar">Daftar</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link active" href="/hasil">Hasil</a>
                </li>
            </ul>
        </div>
    </nav>
    {{-- end nav --}}
@endsection

@section('content')
    {{-- start content --}}
    <div class="judul">
        <p>Rekap Registrasi Beasiswa</p>
    </div>

    <div class="tabel table-responsive mb-4 container">
        <table class="table table-hover table-light rounded-3 overflow-hidden" id="tbl_user">
            <thead class="table-warning">
                <tr>
                    <th scope="col" class="text-center">No</th>
                    <th scope="col" class="text-center">Nama</th>
                    <th scope="col" class="text-center">Email</th>
                    <th scope="col" class="text-center">Nomor HP</th>
                    <th scope="col" class="text-center">IPK Terakhir</th>
                    <th scope="col" class="text-center">Pilihan Beasiswa</th>
                    <th scope="col" class="text-center">Status Ajuan</th>
                    <th scope="col" class="text-center">Lihat Berkas</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($datas as $data)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $data->nama }}</td>
                        <td>{{ $data->email }}</td>
                        <td>{{ $data->no_hp }}</td>
                        <td>{{ $data->ipk }}</td>
                        <td>{{ $data->beasiswa }}</td>
                        <td>
                            <div class="status">
                                @if ($data->status_ajuan == 'Proses')
                                    <button type="button" class="btn btn-outline-warning btn-sm" disabled>Pending</button>
                                @elseif($data->status_ajuan == 'Diterima')
                                    <button type="button" class="btn btn-outline-success btn-sm" disabled>Diterima</button>
                                @elseif($data->status_ajuan == 'Ditolak')
                                    <button type="button" class="btn btn-outline-danger btn-sm" disabled>Ditolak</button>
                                @endif
                            </div>
                        </td>
                        <td>
                            <a class="btn btn-primary btn-sm" href="{{ url($data->berkas) }}" role="button" target="_blank">Lihat Berkas</a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>

    <input type="hidden" name="jbu" value="{{ $jbu }}" id="jbu">
    <input type="hidden" name="bj" value="{{ $bj }}" id="bj">
    <input type="hidden" name="jbm" value="{{ $jbm }}" id="jbm">

    <div class="grafik pb-4">
        <div class="judul">
            <p>Rekap Registrasi Beasiswa</p>
        </div>
        <div class="programming-stats">
            <div class="chart-container">
                <canvas class="my-chart"></canvas>
            </div>

            <div class="details">
                <ul></ul>
            </div>
        </div>
        <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
        <script src="{{ asset('js/main.js') }}"></script>
    </div>
    {{-- end content --}}
@endsection
