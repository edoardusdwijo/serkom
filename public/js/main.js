var jbu = parseInt(document.getElementById('jbu').value);
var bj = parseInt(document.getElementById('bj').value);
var jbm = parseInt(document.getElementById('jbm').value);

const chartData = {
  labels: ["Jalur Beasiswa Unggul", "Beasiswa Jateng", "Jalur Beasiswa Muda"],
  data: [jbu, bj, jbm],
};

const myChart = document.querySelector(".my-chart");
const ul = document.querySelector(".programming-stats .details ul");

new Chart(myChart, {
  type: "doughnut",
  data: {
    labels: chartData.labels,
    datasets: [
      {
        label: "Language Popularity",
        data: chartData.data,
      },
    ],
  },
  options: {
    borderWidth: 10,
    borderRadius: 2,
    hoverBorderWidth: 0,
    plugins: {
      legend: {
        display: false,
      },
    },
  },
});

const populateUl = () => {
  chartData.labels.forEach((l, i) => {
    let li = document.createElement("li");
    li.innerHTML = `${l}: <span class='percentage'>${chartData.data[i]} Orang</span>`;
    ul.appendChild(li);
  });
};

populateUl();
