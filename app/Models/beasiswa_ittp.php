<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class beasiswa_ittp extends Model
{
    use HasFactory;
    protected $table = 'beasiswa_ittps';
    protected $guarded = ['id'];
}
