<?php

namespace App\Http\Controllers;

use App\Models\beasiswa_ittp;
use Illuminate\Http\Request;

class regist_beasiswa extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'email' => 'required',
            'no_hp' => 'required',
            'semester' => 'required',
            'ipk' => 'required',
            'beasiswa' => 'required',
            'berkas' => 'required|max:5000|mimes:pdf,zip,jpg',
        ]);

        // Mengecek apakah email sudah terdaftar di database
        $emailExists = beasiswa_ittp::where('email', $request->email)->exists();
        if ($emailExists) {
            return redirect('daftar');
        }
        // menyimpan file ke dalam direktori server
        $file = $request->file('berkas');
        $nama_file = time() . "_" . $file->getClientOriginalName();
        $tujuan_upload = $file->store('public');
        $file->move($tujuan_upload, $nama_file);

        $data = new beasiswa_ittp();
        $data->nama = $request->nama;
        $data->email = $request->email;
        $data->no_hp = $request->no_hp;
        $data->semester = $request->semester;
        $data->ipk = $request->ipk;
        $data->beasiswa = $request->beasiswa;
        $data->berkas = $tujuan_upload . '/' . $nama_file;
        $data->save();

        return redirect('daftar');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function hasil()
    {
        $datas = beasiswa_ittp::all();

        $jbu = beasiswa_ittp::where('beasiswa', 'Jalur Beasiswa Unggul')->count();
        $bj = beasiswa_ittp::where('beasiswa', 'Beasiswa Jateng')->count();
        $jbm = beasiswa_ittp::where('beasiswa', 'Jalur Beasiswa Muda')->count();
        return view('hasil', compact('datas', 'jbu', 'bj', 'jbm'));
    }
}
